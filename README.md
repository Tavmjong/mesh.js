# SVG Mesh Gradient Polyfill

SVG mesh gradients allow real life shadings. They are supported in the latest version of [Inkscape](https://inkscape.org) (0.92).

Until browsers have implemented meshes natively, one can use this JavaScript polyfill to render them.

Just before the closing tag in your file:

* For HTML:

    	    <script src="mesh.js"></script>

* For SVG:

    	    <script type="text/javascript" xlink:href="mesh.js"></script>
    	  

and include _mesh.js_ in the same directory as your file.

Note that mesh.js is a work in progress written by a novice at Javascript. It has some limitations:

* No support for bi-cubic blending.
* No support for meshes on strokes.
* Limited attempt to make the code efficient.
* Occasional rendering errors (e.g. some unfilled pixels in rotated meshes).

Mesh rendering, of course, would be much better handled natively in browsers. Let them know if you want native rendering! 
